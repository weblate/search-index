import * as express from 'express'
import { Searcher } from '../../lib/controllers/searcher'
import { formatEventForAPI, queryEvents } from '../../lib/elastic-search/elastic-search-events'
import { asyncMiddleware } from '../../middlewares/async'
import { setDefaultPagination } from '../../middlewares/pagination'
import { setDefaultSearchSort } from '../../middlewares/sort'
import { methodsValidator } from '../../middlewares/validators/method'
import { paginationValidator } from '../../middlewares/validators/pagination'
import { commonFiltersValidators, commonEventsFiltersValidator, eventsSearchValidator } from '../../middlewares/validators/search'
import { eventsSearchSortValidator } from '../../middlewares/validators/sort'
import { EventsSearchQuery } from '../../types/search-query/event-search.model'

const searchEventsRouter = express.Router()

searchEventsRouter.all('/search/events',
  methodsValidator(['POST', 'GET']),
  paginationValidator,
  setDefaultPagination,
  eventsSearchSortValidator,
  setDefaultSearchSort,
  commonFiltersValidators,
  commonEventsFiltersValidator,
  eventsSearchValidator,
  asyncMiddleware(searchEvents)
)

// ---------------------------------------------------------------------------

export { searchEventsRouter }

// ---------------------------------------------------------------------------

async function searchEvents (req: express.Request, res: express.Response) {
  const query = Object.assign(req.query || {}, req.body || {}) as EventsSearchQuery
  const searcher = new Searcher(queryEvents, formatEventForAPI)
  const result = await searcher.getResult(query)

  return res.json(result)
}
