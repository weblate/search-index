import * as express from 'express'
import { Searcher } from '../../lib/controllers/searcher'
import { formatGroupForAPI, queryGroups } from '../../lib/elastic-search/elastic-search-groups'
import { asyncMiddleware } from '../../middlewares/async'
import { setDefaultPagination } from '../../middlewares/pagination'
import { setDefaultSearchSort } from '../../middlewares/sort'
import { methodsValidator } from '../../middlewares/validators/method'
import { paginationValidator } from '../../middlewares/validators/pagination'
import { commonFiltersValidators, groupsSearchValidator } from '../../middlewares/validators/search'
import { groupsSearchSortValidator } from '../../middlewares/validators/sort'
import { GroupsSearchQuery } from '../../types/search-query/group-search.model'

const searchGroupsRouter = express.Router()

searchGroupsRouter.all('/search/groups',
  methodsValidator(['POST', 'GET']),
  paginationValidator,
  setDefaultPagination,
  groupsSearchSortValidator,
  setDefaultSearchSort,
  commonFiltersValidators,
  groupsSearchValidator,
  asyncMiddleware(searchGroups)
)

// ---------------------------------------------------------------------------

export { searchGroupsRouter }

// ---------------------------------------------------------------------------

async function searchGroups (req: express.Request, res: express.Response) {
  const query = Object.assign(req.query || {}, req.body || {}) as GroupsSearchQuery

  if (query.handles && query.fromHost) {
    query.handles = query.handles.map(h => {
      if (h.includes('@')) return h

      return h + '@' + query.fromHost
    })
  }

  const searcher = new Searcher(queryGroups, formatGroupForAPI)
  const result = await searcher.getResult(query)

  return res.json(result)
}
