import * as express from 'express'
import { Searcher } from '../../lib/controllers/searcher'
import { formatPostForAPI, queryPosts } from '../../lib/elastic-search/elastic-search-posts'
import { asyncMiddleware } from '../../middlewares/async'
import { setDefaultPagination } from '../../middlewares/pagination'
import { setDefaultSearchSort } from '../../middlewares/sort'
import { methodsValidator } from '../../middlewares/validators/method'
import { paginationValidator } from '../../middlewares/validators/pagination'
import { commonFiltersValidators, postsSearchValidator } from '../../middlewares/validators/search'
import { postsSearchSortValidator } from '../../middlewares/validators/sort'
import { PostsSearchQuery } from '../../types/search-query/post-search.model'

const searchPostsRouter = express.Router()

searchPostsRouter.all('/search/posts',
  methodsValidator(['POST', 'GET']),
  paginationValidator,
  setDefaultPagination,
  postsSearchSortValidator,
  setDefaultSearchSort,
  commonFiltersValidators,
  postsSearchValidator,
  asyncMiddleware(searchPosts)
)

// ---------------------------------------------------------------------------

export { searchPostsRouter }

// ---------------------------------------------------------------------------

async function searchPosts (req: express.Request, res: express.Response) {
  const query = Object.assign(req.query || {}, req.body || {}) as PostsSearchQuery

  const searcher = new Searcher(queryPosts, formatPostForAPI)
  const result = await searcher.getResult(query)

  return res.json(result)
}
