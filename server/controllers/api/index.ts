import * as express from 'express'
import { badRequest } from '../../helpers/utils'
import { configRouter } from './config'
import { searchGroupsRouter } from './search-groups'
import { searchPostsRouter } from './search-posts'
import { searchEventsRouter } from './search-events'
import { searchLocationsRouter } from './search-locations'

const apiRouter = express.Router()

apiRouter.use('/', configRouter)
apiRouter.use('/', searchEventsRouter)
apiRouter.use('/', searchGroupsRouter)
apiRouter.use('/', searchPostsRouter)
apiRouter.use('/', searchLocationsRouter)
apiRouter.use('/ping', pong)
apiRouter.use('/*', badRequest)

// ---------------------------------------------------------------------------

export { apiRouter }

// ---------------------------------------------------------------------------

function pong (req: express.Request, res: express.Response) {
  return res.send('pong').status(200).end()
}
