import { SORTABLE_COLUMNS } from '../../initializers/constants'
import { checkSort, createSortableColumns } from './utils'

const SORTABLE_EVENTS_SEARCH_COLUMNS = createSortableColumns(SORTABLE_COLUMNS.EVENTS_SEARCH)
const SORTABLE_GROUPS_SEARCH_COLUMNS = createSortableColumns(SORTABLE_COLUMNS.GROUPS_SEARCH)
const SORTABLE_POSTS_SEARCH_COLUMNS = createSortableColumns(SORTABLE_COLUMNS.POSTS_SEARCH)

const eventsSearchSortValidator = checkSort(SORTABLE_EVENTS_SEARCH_COLUMNS)
const groupsSearchSortValidator = checkSort(SORTABLE_GROUPS_SEARCH_COLUMNS)
const postsSearchSortValidator = checkSort(SORTABLE_POSTS_SEARCH_COLUMNS)

// ---------------------------------------------------------------------------

export {
  eventsSearchSortValidator,
  groupsSearchSortValidator,
  postsSearchSortValidator
}
