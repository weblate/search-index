import * as express from 'express'
import { check } from 'express-validator'
import { areCoordinatesValid, isDateValid, isDistanceValid, toArray } from '../../helpers/custom-validators/misc'
import { isNSFWQueryValid, isNumberArray, isStringArray } from '../../helpers/custom-validators/search-events'
import { logger } from '../../helpers/logger'
import { areValidationErrors } from './utils'

const commonFiltersValidators = [
  check('blockedAccounts')
    .optional()
    .customSanitizer(toArray)
    .custom(isStringArray).withMessage('Should have a valid blockedAccounts array'),
  check('blockedHosts')
    .optional()
    .customSanitizer(toArray)
    .custom(isStringArray).withMessage('Should have a valid hosts array'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ query: req.query, body: req.body }, 'Checking commons filters query')

    if (areValidationErrors(req, res)) return

    return next()
  }
]

const commonEventsFiltersValidator = [
  check('categoryOneOf')
    .optional()
    .customSanitizer(toArray)
    .custom(isNumberArray).withMessage('Should have a valid one of category array'),
  check('licenceOneOf')
    .optional()
    .customSanitizer(toArray)
    .custom(isNumberArray).withMessage('Should have a valid one of licence array'),
  check('languageOneOf')
    .optional()
    .customSanitizer(toArray)
    .custom(isStringArray).withMessage('Should have a valid one of language array'),
  check('tagsOneOf')
    .optional()
    .customSanitizer(toArray)
    .custom(isStringArray).withMessage('Should have a valid one of tags array'),
  check('tagsAllOf')
    .optional()
    .customSanitizer(toArray)
    .custom(isStringArray).withMessage('Should have a valid all of tags array'),
  check('nsfw')
    .optional()
    .custom(isNSFWQueryValid).withMessage('Should have a valid NSFW attribute'),
  check('latlon')
    .optional()
    .custom(areCoordinatesValid).withMessage('Should have valid coordinates'),
  check('distance')
    .optional()
    .custom(isDistanceValid).withMessage('Should have valid distance')
    .rtrim('_km'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (areValidationErrors(req, res)) return

    return next()
  }
]

const eventsSearchValidator = [
  check('search').optional().not().isEmpty().withMessage('Should have a valid search'),

  check('host').optional().not().isEmpty().withMessage('Should have a valid host'),

  check('startDateMin').optional().custom(isDateValid).withMessage('Should have a valid minimum start date'),
  check('startDateMax').optional().custom(isDateValid).withMessage('Should have a valid maximum start date'),

  check('boostLanguages')
    .optional()
    .customSanitizer(toArray)
    .custom(isStringArray).withMessage('Should have a valid boostLanguages array'),

  check('uuids')
    .optional()
    .toArray(),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ query: req.query, body: req.body }, 'Checking events search query')

    if (areValidationErrors(req, res)) return

    return next()
  }
]

const groupsSearchValidator = [
  check('search').optional().not().isEmpty().withMessage('Should have a valid search'),
  check('host').optional().not().isEmpty().withMessage('Should have a valid host'),
  check('handles').optional().toArray(),
  check('latlon')
    .optional()
    .custom(areCoordinatesValid).withMessage('Should have valid coordinates'),
  check('distance')
    .optional()
    .custom(isDistanceValid).withMessage('Should have valid distance')
    .rtrim('_km'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ query: req.query, body: req.body }, 'Checking groups search query')

    if (areValidationErrors(req, res)) return

    return next()
  }
]

const postsSearchValidator = [
  check('search').optional().not().isEmpty().withMessage('Should have a valid search'),

  check('host').optional().not().isEmpty().withMessage('Should have a valid host'),

  check('uuids')
    .optional()
    .toArray(),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ query: req.query, body: req.body }, 'Checking posts search query')

    if (areValidationErrors(req, res)) return

    return next()
  }
]

const locationsSearchValidator = [
  check('search').optional().not().isEmpty().withMessage('Should have a valid search'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ query: req.query, body: req.body }, 'Checking location search query')

    if (areValidationErrors(req, res)) {
      logger.info('Field validation error')
      return
    }

    return next()
  }
]

// ---------------------------------------------------------------------------

export {
  groupsSearchValidator,
  commonFiltersValidators,
  commonEventsFiltersValidator,
  postsSearchValidator,
  eventsSearchValidator,
  locationsSearchValidator
}
