import 'express-validator'
import * as express from 'express'
import { PAGINATION_COUNT } from '../initializers/constants'

function setDefaultPagination (req: express.Request, res: express.Response, next: express.NextFunction) {
  if (!req.query.start) req.query.start = '0'
  else req.query.start = parseInt(req.query.start as string, 10).toString()

  if (!req.query.count) req.query.count = PAGINATION_COUNT.DEFAULT.toString()
  else req.query.count = parseInt(req.query.count as string, 10).toString()

  return next()
}

// ---------------------------------------------------------------------------

export { setDefaultPagination }
