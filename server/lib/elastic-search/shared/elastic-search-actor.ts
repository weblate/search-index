import { Actor, APActor, DBActor } from 'server/types/actor.model'

function buildActorSummaryMapping () {
  return {
    id: {
      type: 'binary'
    },

    name: {
      type: 'text',
      fields: {
        raw: {
          type: 'keyword'
        }
      }
    },
    displayName: {
      type: 'text'
    },
    url: {
      type: 'keyword'
    },
    host: {
      type: 'keyword'
    },
    handle: {
      type: 'keyword'
    },
    avatar: {
      type: 'binary'
    }
  }
}

function buildActorCommonMapping () {
  return {
    ...buildActorSummaryMapping(),

    createdAt: {
      type: 'date',
      format: 'date_optional_time'
    },
    updatedAt: {
      type: 'date',
      format: 'date_optional_time'
    },

    description: {
      type: 'text'
    }
  }
}

function formatActorSummaryForAPI (actor: Actor): Omit<DBActor, 'handle'> | null {
  // console.log('formatActorSummaryForAPI', actor)
  if (!actor) return null
  const url = new URL(actor.id)
  return {
    id: actor.id,
    name: actor.name,
    displayName: actor.displayName,
    url: actor.id,
    host: url.host,
    description: actor.description,
    avatar: actor.avatar
  }
}

function formatActorForDB (actor: APActor): DBActor {
  if (actor.id) {
    const url = new URL(actor.id)
    return {
      id: actor.id,
      name: actor.preferredUsername,
      displayName: actor.name,
      url: actor.id,
      host: url.host,
      description: actor.summary,

      handle: `${actor.name}@${url.host}`,

      avatar: actor.icon?.url
    }
  }
}

export {
  buildActorCommonMapping,
  buildActorSummaryMapping,
  formatActorSummaryForAPI,
  formatActorForDB
}
