import validator from 'validator'

function addUUIDFilters (filters: any[], uuids: string[]) {
  if (!filters) return

  const result = {
    shortUUIDs: [] as string[],
    uuids: [] as string[]
  }

  for (const uuid of uuids) {
    if (validator.isUUID(uuid)) result.uuids.push(uuid)
    else result.shortUUIDs.push(uuid)
  }

  filters.push({
    bool: {
      should: [
        {
          terms: {
            uuid: result.uuids
          }
        },
        {
          terms: {
            shortUUID: result.shortUUIDs
          }
        }
      ]
    }
  })
}

export {
  addUUIDFilters
}
