import { elasticSearch } from '../../helpers/elastic-search'
import { logger } from '../../helpers/logger'
import { CONFIG, ELASTIC_SEARCH_QUERY } from '../../initializers/constants'
import { EventsSearchQuery } from '../../types/search-query/event-search.model'
import { DBEvent, EnhancedEvent, IndexableEvent } from '../../types/event.model'
import { buildSort, extractQueryResult } from './elastic-search-queries'
import { addUUIDFilters } from './shared'
import { buildActorSummaryMapping, formatActorForDB, formatActorSummaryForAPI } from './shared/elastic-search-actor'
import { buildLocationSummaryMapping, formatLocationForDB } from './shared/elastic-search-address'

async function queryEvents (search: EventsSearchQuery) {
  const bool: any = {}
  const filter: any[] = []
  const mustNot: any[] = []

  let lat, lon
  if (search.latlon) {
    [lat, lon] = search.latlon.split(':')
  }

  if (search.search) {
    Object.assign(bool, {
      must: [
        {
          multi_match: {
            query: search.search,
            fields: ELASTIC_SEARCH_QUERY.EVENTS_MULTI_MATCH_FIELDS,
            fuzziness: ELASTIC_SEARCH_QUERY.FUZZINESS
          }
        }
      ]
    })
  }

  if (search.blockedAccounts) {
    mustNot.push({
      terms: {
        'creator.handle': search.blockedAccounts
      }
    })

    mustNot.push({
      terms: {
        'group.handle': search.blockedAccounts
      }
    })
  }

  if (search.blockedHosts) {
    mustNot.push({
      terms: {
        host: search.blockedHosts
      }
    })
  }

  if (search.startDate) {
    filter.push({
      range: {
        publishedAt: {
          gte: search.startDate
        }
      }
    })
  }

  if (search.endDate) {
    filter.push({
      range: {
        publishedAt: {
          lte: search.endDate
        }
      }
    })
  }

  // if (search.categoryOneOf) {
  //   filter.push({
  //     terms: {
  //       'category.id': search.categoryOneOf
  //     }
  //   })
  // }

  if (search.languageOneOf) {
    filter.push({
      terms: {
        'language.id': search.languageOneOf
      }
    })
  }

  if (search.tagsOneOf) {
    filter.push({
      terms: {
        tags: search.tagsOneOf
      }
    })
  }

  if (search.tagsAllOf) {
    for (const t of search.tagsAllOf) {
      filter.push({
        term: {
          tags: t
        }
      })
    }
  }

  if (search.host) {
    filter.push({
      term: {
        'creator.host': search.host
      }
    })
  }

  if (search.startDateMin) {
    filter.push({
      range: {
        startTime: {
          gte: search.startDateMin
        }
      }
    })
  }

  if (search.startDateMax) {
    filter.push({
      range: {
        startTime: {
          lte: search.startDateMax
        }
      }
    })
  }

  if (lat && lon && search.distance && search.distance !== 'anywhere') {
    filter.push({
      geo_distance: {
        distance: `${search.distance}km`,
        'location.location': {
          lat,
          lon
        }
      }
    })
  }

  if (search.uuids) {
    addUUIDFilters(filter, search.uuids)
  }

  Object.assign(bool, { filter })

  if (mustNot.length !== 0) {
    Object.assign(bool, { must_not: mustNot })
  }

  const body = {
    from: search.start,
    size: search.count,
    sort: buildSort(search.sort)
  }

  // Allow to boost results depending on query languages
  if (
    CONFIG.EVENTS_SEARCH.BOOST_LANGUAGES.ENABLED &&
    Array.isArray(search.boostLanguages) &&
    search.boostLanguages.length !== 0
  ) {
    const boostScript = `
      if (doc['language.id'].size() == 0) {
        return _score;
      }

      String language = doc['language.id'].value;

      for (String docLang: params.boostLanguages) {
        if (docLang == language) return _score * params.boost;
      }

      return _score;
    `

    Object.assign(body, {
      query: {
        script_score: {
          query: { bool },
          script: {
            source: boostScript,
            params: {
              boostLanguages: search.boostLanguages,
              boost: ELASTIC_SEARCH_QUERY.BOOST_LANGUAGE_VALUE
            }
          }
        }
      }
    })
  } else {
    Object.assign(body, { query: { bool } })
  }

  logger.debug({ body }, 'Will query Elastic Search for events.')

  const res = await elasticSearch.search({
    index: CONFIG.ELASTIC_SEARCH.INDEXES.EVENTS,
    body
  })

  return extractQueryResult(res)
}

function buildEventsMapping () {
  return {
    id: {
      type: 'binary'
    },

    uuid: {
      type: 'keyword'
    },
    shortUUID: {
      type: 'keyword'
    },
    createdAt: {
      type: 'date',
      format: 'date_optional_time'
    },
    updatedAt: {
      type: 'date',
      format: 'date_optional_time'
    },
    publishedAt: {
      type: 'date',
      format: 'date_optional_time'
    },
    indexedAt: {
      type: 'date',
      format: 'date_optional_time'
    },

    // category: {
    //   properties: {
    //     id: {
    //       type: 'keyword'
    //     },
    //     label: {
    //       type: 'text'
    //     }
    //   }
    // },

    language: {
      properties: {
        id: {
          type: 'keyword'
        },
        label: {
          type: 'text'
        }
      }
    },

    name: {
      type: 'text'
    },

    description: {
      type: 'text'
    },

    startTime: {
      type: 'date',
      format: 'date_optional_time'
    },

    endTime: {
      type: 'date',
      format: 'date_optional_time'
    },

    tags: {
      type: 'text',

      fields: {
        raw: {
          type: 'keyword'
        }
      }
    },

    // thumbnailPath: {
    //   type: 'keyword'
    // },

    url: {
      type: 'keyword'
    },

    host: {
      type: 'keyword'
    },

    creator: {
      properties: buildActorSummaryMapping()
    },

    group: {
      properties: buildActorSummaryMapping()
    },

    location: {
      properties: buildLocationSummaryMapping()
    }
  }
}

function formatEventForDB (e: IndexableEvent): DBEvent {
  const banner = e.attachment.find((a) => a.name === 'Banner')
  return {
    id: e.id,
    uuid: e.uuid,

    indexedAt: new Date(),
    updated: e.updated,
    published: e.published,

    // language: {
    //   id: e.language.id,
    //   label: e.language.label
    // },
    // privacy: {
    //   id: e.privacy.id,
    //   label: e.privacy.label
    // },

    name: e.name,
    // description: e.content,
    endTime: e.endTime,
    startTime: e.startTime,
    status: e['ical:status'],
    // thumbnailPath: e.thumbnailPath,
    // previewPath: e.previewPath,

    host: e.host,
    url: e.url,

    tags: e.tag.map(({ name }: { name: string }) => name),
    location: formatLocationForDB(e.location),

    creator: formatActorForDB(e.actor),
    group: e.attributedTo && e.attributedTo.id !== e.actor.id ? formatActorForDB(e.attributedTo) : null,
    banner: banner ? banner.url : null
  }
}

function formatEventForAPI (e: DBEvent, fromHost?: string): EnhancedEvent {
  // console.log('formatEventForAPI')
  // console.log(e)
  return {
    id: e.id,
    uuid: e.uuid,

    score: e.score,

    // createdAt: new Date(v.createdAt),
    // updated: new Date(v.updated),
    // published: new Date(v.published),

    // category: {
    //   id: v.category.id,
    //   label: v.category.label
    // },

    // language: {
    //   id: v.language.id,
    //   label: v.language.label
    // },
    // privacy: {
    //   id: v.privacy.id,
    //   label: v.privacy.label
    // },

    name: e.name,
    endTime: e.endTime,
    startTime: e.startTime,
    status: e['ical:status'],
    published: e.published,
    tags: e.tags,
    updated: e.updated,
    location: e.location,

    // thumbnailPath: v.thumbnailPath,
    // thumbnailUrl: buildUrl(v.host, v.thumbnailPath),

    // previewPath: v.previewPath,
    // previewUrl: buildUrl(v.host, v.previewPath),

    url: e.url,

    // isLocal: fromHost && fromHost === v.host,

    creator: formatActorSummaryForAPI(e.creator),
    group: formatActorSummaryForAPI(e.group),
    banner: e.banner
  }
}

export { queryEvents, formatEventForDB, formatEventForAPI, buildEventsMapping }
