import { elasticSearch } from '../../helpers/elastic-search'
import { logger } from '../../helpers/logger'
import { CONFIG, ELASTIC_SEARCH_QUERY } from '../../initializers/constants'
import { DBPost, EnhancedPost, IndexablePost } from '../../types/post.model'
import { PostsSearchQuery } from '../../types/search-query/post-search.model'
import { buildSort, extractQueryResult } from './elastic-search-queries'
import { addUUIDFilters } from './shared'
import { buildActorSummaryMapping, formatActorForDB, formatActorSummaryForAPI } from './shared/elastic-search-actor'

async function queryPosts (search: PostsSearchQuery) {
  const bool: any = {}
  const mustNot: any[] = []
  const filter: any[] = []

  if (search.search) {
    Object.assign(bool, {
      must: [
        {
          multi_match: {
            query: search.search,
            fields: ELASTIC_SEARCH_QUERY.POSTS_MULTI_MATCH_FIELDS,
            fuzziness: ELASTIC_SEARCH_QUERY.FUZZINESS
          }
        }
      ]
    })
  }

  if (search.blockedAccounts) {
    mustNot.push({
      terms: {
        'ownerAccount.handle': search.blockedAccounts
      }
    })
  }

  if (search.blockedHosts) {
    mustNot.push({
      terms: {
        host: search.blockedHosts
      }
    })
  }

  if (search.host) {
    filter.push({
      term: {
        'ownerAccount.host': search.host
      }
    })
  }

  if (search.uuids) {
    addUUIDFilters(filter, search.uuids)
  }

  if (filter.length !== 0) {
    Object.assign(bool, { filter })
  }

  if (mustNot.length !== 0) {
    Object.assign(bool, { must_not: mustNot })
  }

  const body = {
    from: search.start,
    size: search.count,
    sort: buildSort(search.sort),
    query: { bool }
  }

  logger.debug({ body }, 'Will query Elastic Search for posts.')

  const res = await elasticSearch.search({
    index: CONFIG.ELASTIC_SEARCH.INDEXES.POSTS,
    body
  })

  return extractQueryResult(res)
}

function formatPostForAPI (p: DBPost, fromHost?: string): EnhancedPost {
  return {
    id: p.id,
    uuid: p.uuid,

    score: p.score,

    url: p.url,

    title: p.title,
    description: p.description,
    updated: p.updated,
    tags: p.tags,
    published: p.published,

    creator: formatActorSummaryForAPI(p.creator),
    group: formatActorSummaryForAPI(p.group)
  }
}

function formatPostForDB (p: IndexablePost): DBPost {
  return {
    id: p.id,
    uuid: p.uuid,

    indexedAt: new Date(),
    updated: p.updated,

    host: p.host,
    url: p.url,

    title: p.name,
    description: p.content,

    // thumbnailPath: p.thumbnailPath,

    // type: {
    //   id: p.type.id,
    //   label: p.type.label
    // },
    // privacy: {
    //   id: p.privacy.id,
    //   label: p.privacy.label
    // },
    tags: p.tag,
    published: p.published,

    creator: formatActorForDB(p.actor),
    group: formatActorForDB(p.attributedTo)
  }
}

function buildPostsMapping () {
  return {
    id: {
      type: 'long'
    },

    uuid: {
      type: 'keyword'
    },
    shortUUID: {
      type: 'keyword'
    },
    createdAt: {
      type: 'date',
      format: 'date_optional_time'
    },
    updatedAt: {
      type: 'date',
      format: 'date_optional_time'
    },
    indexedAt: {
      type: 'date',
      format: 'date_optional_time'
    },

    privacy: {
      properties: {
        id: {
          type: 'keyword'
        },
        label: {
          type: 'text'
        }
      }
    },

    displayName: {
      type: 'text'
    },

    description: {
      type: 'text'
    },

    thumbnailPath: {
      type: 'keyword'
    },
    embedPath: {
      type: 'keyword'
    },

    url: {
      type: 'keyword'
    },

    host: {
      type: 'keyword'
    },

    creator: {
      properties: buildActorSummaryMapping()
    },

    group: {
      properties: buildActorSummaryMapping()
    }
  }
}

export {
  formatPostForAPI,
  buildPostsMapping,
  formatPostForDB,
  queryPosts
}
