
import { toSafeHtml } from '../../helpers/sanitizer'
import { elasticSearch } from '../../helpers/elastic-search'
import { logger } from '../../helpers/logger'
import { CONFIG, ELASTIC_SEARCH_QUERY } from '../../initializers/constants'
import { DBGroup, EnhancedGroup, IndexableGroup } from '../../types/group.model'
import { GroupsSearchQuery } from '../../types/search-query/group-search.model'
import { buildSort, extractQueryResult } from './elastic-search-queries'
import { buildActorCommonMapping } from './shared'
import { buildLocationSummaryMapping, formatLocationForDB } from './shared/elastic-search-address'

async function queryGroups (search: GroupsSearchQuery) {
  const bool: any = {}
  const mustNot: any[] = []
  const filter: any[] = []

  let lat, lon
  if (search.latlon) {
    [lat, lon] = search.latlon.split(':')
  }

  if (search.search) {
    Object.assign(bool, {
      must: [
        {
          multi_match: {
            query: search.search,
            fields: ELASTIC_SEARCH_QUERY.GROUPS_MULTI_MATCH_FIELDS,
            fuzziness: ELASTIC_SEARCH_QUERY.FUZZINESS
          }
        }
      ]
    })
  }

  if (search.blockedHosts) {
    mustNot.push({
      terms: {
        host: search.blockedHosts
      }
    })
  }

  if (search.host) {
    filter.push({
      term: {
        host: search.host
      }
    })
  }

  if (search.handles) {
    filter.push({
      terms: {
        handle: search.handles
      }
    })
  }

  if (lat && lon && search.distance && search.distance !== 'anywhere') {
    filter.push({
      geo_distance: {
        distance: `${search.distance}km`,
        'location.location': {
          lat,
          lon
        }
      }
    })
  }

  if (filter.length !== 0) {
    Object.assign(bool, { filter })
  }

  if (mustNot.length !== 0) {
    Object.assign(bool, { must_not: mustNot })
  }

  const body = {
    from: search.start,
    size: search.count,
    sort: buildSort(search.sort),
    query: { bool }
  }

  logger.debug({ body }, 'Will query Elastic Search for groups.')

  const res = await elasticSearch.search({
    index: CONFIG.ELASTIC_SEARCH.INDEXES.GROUPS,
    body
  })

  return extractQueryResult(res)
}

function formatGroupForAPI (c: DBGroup, fromHost?: string): EnhancedGroup {
  return {
    id: c.id,

    score: c.score,

    // url: c.id,
    name: c.name,
    // host: c.host,
    avatar: c.avatar,

    displayName: c.displayName,
    description: c.description,
    url: c.id,
    location: c.location,
    host: c.host
  }
}

function formatGroupForDB (c: IndexableGroup): DBGroup {
  console.log('formatgroupfordb', c.location)
  return {
    id: c.id,

    name: c.preferredUsername,
    host: c.host,
    url: c.url,

    avatar: c.icon?.url,

    displayName: c.name,

    indexedAt: new Date(),
    description: toSafeHtml(c.summary),
    location: formatLocationForDB(c.location),

    handle: c.handle
  }
}

function buildGroupsMapping () {
  const base = buildActorCommonMapping()

  return {
    ...base,
    location: {
      properties: buildLocationSummaryMapping()
    }
  }
}

export {
  buildGroupsMapping,
  formatGroupForDB,
  formatGroupForAPI,
  queryGroups
}
