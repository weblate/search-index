import { search as peliasSearch } from './providers/pelias'
import { CONFIG } from '../../initializers/constants'
import { ResultList } from '../../types/search-query/common-search.model'
import { AutoCompleteLocation } from '../../types/address.model'

export async function search (location: string): Promise<ResultList<AutoCompleteLocation>> {
  switch (CONFIG.GEOCODING.TYPE) {
    case 'pelias':
      return await peliasSearch(location, {
        endpoint: CONFIG.GEOCODING.ENDPOINT,
        userAgent: CONFIG.GEOCODING.USER_AGENT
      })
    default:
      throw new Error('No location provider registered')
  }
}
