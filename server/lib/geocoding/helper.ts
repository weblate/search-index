import { FeatureCollection, Point } from 'geojson'
import { ResultList } from '../../types/search-query/common-search.model'
import { AutoCompleteLocation } from '../../types/address.model'

export function geoJSONToResult (geoJSON: FeatureCollection<Point>): ResultList<AutoCompleteLocation> {
  const rawData = geoJSON.features.map(({ geometry: { coordinates: [lon, lat] }, properties: { name } }) => ({ location: { lon, lat }, name }))
  return { total: rawData.length, data: rawData }
}
