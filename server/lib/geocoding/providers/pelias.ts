import axios from 'axios'
import { ProviderOptions } from '../../../types/providers'
import { FeatureCollection, Point } from 'geojson'
import { ResultList } from '../../../types/search-query/common-search.model'
import { AutoCompleteLocation } from '../../../types/address.model'
import { geoJSONToResult } from '../helper'
import { logger } from '../../../helpers/logger'

export const PELIAS_AUTOCOMPLETE_API = '/v1/autocomplete'

export async function search (location: string, options: ProviderOptions): Promise<ResultList<AutoCompleteLocation>> {
  const url = `${options.endpoint}${PELIAS_AUTOCOMPLETE_API}?text=${location}&layers=coarse`
  try {
    console.log(url)
    const res = await axios.get<FeatureCollection<Point>>(url)
    console.log(res.data.features[0].properties)
    return geoJSONToResult(res.data)
  } catch (err) {
    logger.error({ err }, 'Failure fetching location details from Pelias')
  }
}
