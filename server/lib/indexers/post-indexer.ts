import { CONFIG } from '../../initializers/constants'
import { DBPost, IndexablePost } from '../../types/post.model'
import { buildPostsMapping, formatPostForDB } from '../elastic-search/elastic-search-posts'
import { AbstractIndexer } from './shared'

export class PostIndexer extends AbstractIndexer <IndexablePost, DBPost> {

  constructor () {
    super(CONFIG.ELASTIC_SEARCH.INDEXES.POSTS, formatPostForDB)
  }

  async indexSpecificElement (host: string, uuid: string) {
    // We don't need to index a specific element yet, since we have all playlist information in the list endpoint
    throw new Error('Not implemented')
  }

  buildMapping () {
    return buildPostsMapping()
  }
}
