import { logger } from '../../helpers/logger'
import { CONFIG } from '../../initializers/constants'
import { DBGroup, IndexableGroup } from '../../types/group.model'
import { buildGroupsMapping, formatGroupForDB } from '../elastic-search/elastic-search-groups'
import { getGroup } from '../requests/mobilizon-instance'
import { AbstractIndexer } from './shared'

export class GroupIndexer extends AbstractIndexer <IndexableGroup, DBGroup> {

  constructor () {
    super(CONFIG.ELASTIC_SEARCH.INDEXES.GROUPS, formatGroupForDB)

    this.indexQueue.drain(async () => {
      logger.info('Refresh groups index.')

      await this.refreshIndex()
    })
  }

  async indexSpecificElement (host: string, name: string) {
    const group = await getGroup(host, name)

    logger.info('Indexing specific group %s@%s.', name, host)

    return this.indexElements([ group ], true)
  }

  buildMapping () {
    return buildGroupsMapping()
  }
}
