import { APPost, IndexablePost } from 'server/types/post.model'
import { doRequestWithRetries } from '../../helpers/requests'
import { INDEXER_COUNT, REQUESTS } from '../../initializers/constants'
import { IndexableGroup } from '../../types/group.model'
import { IndexableDoc } from '../../types/indexable-doc.model'
import { APEvent, IndexableEvent } from '../../types/event.model'
import { APObject, OrderedCollectionPage } from 'server/types/activity-pub'
import { APActor, APGroup } from 'server/types/actor.model'
import { logger } from '../../helpers/logger'

async function getActor (url: string): Promise<APActor> {
  logger.debug(`Getting actor at ${url}`)
  const res = await doRequestWithRetries<any>({
    uri: url,
    headers: {
      Accept: 'application/activity+json'
    },
    json: true
  }, REQUESTS.MAX_RETRIES, REQUESTS.WAIT)

  return res.body
}

async function getEvent (host: string, uuid: string): Promise<IndexableEvent> {
  const url = 'https://' + host + '/events/' + uuid
  logger.debug(`Getting event at ${url}`)

  const res = await doRequestWithRetries<any>({
    uri: url,
    headers: {
      Accept: 'application/activity+json'
    },
    json: true
  }, REQUESTS.MAX_RETRIES, REQUESTS.WAIT)

  return prepareEventForDB(res.body, host)
}

async function getGroup (host: string, name: string): Promise<IndexableGroup> {
  const res = await doRequestWithRetries<APGroup>({
    uri: name,
    headers: {
      Accept: 'application/activity+json'
    },
    json: true
  }, REQUESTS.MAX_RETRIES, REQUESTS.WAIT)

  return prepareGroupForDB(res.body, host)
}

async function fetchOutbox (host: string, page: number): Promise<{events: IndexableEvent[], posts: IndexablePost[]}> {
  const url = 'https://' + host + '/@relay/outbox'

  const res = await doRequestWithRetries<OrderedCollectionPage<APEvent|APPost>>({
    uri: url,
    qs: {
      page,
      filter: 'local',
      nsfw: 'both',
      skipCount: true,
      count: INDEXER_COUNT
    },
    headers: {
      Accept: 'application/activity+json'
    },
    json: true
  }, REQUESTS.MAX_RETRIES, REQUESTS.WAIT)

  let items = res.body.orderedItems.map(i => i.object).filter(i => filterLocal(i, host))
  items = await Promise.all(items.map(async e => await fetchAllRelations(e)))
  const results: {events: APEvent[], posts: APPost[]} = items.reduce((acc, item) => {
    if (item.type === 'Event') {
      acc.events.push(item)
    }
    if (item.type === 'Post') {
      acc.posts.push(item)
    }
    return acc
  }, { events: [], posts: [] })

  return {
    events: results.events.map(e => prepareEventForDB(e, host)),
    posts: results.posts.map(e => preparePostForDB(e, host))
  }
}

function prepareEventForDB <T extends APEvent> (event: T, host: string): APEvent & IndexableDoc {
  return Object.assign(event, {
    elasticSearchId: `${host}:${event.uuid}`,
    host,
    url: 'https://' + host + '/events/' + event.uuid
  })
}

function preparePostForDB <T extends APPost> (post: T, host: string): APPost & IndexableDoc {
  return Object.assign(post, {
    elasticSearchId: `${host}:${post.uuid}`,
    host,
    url: 'https://' + host + '/p/' + post.uuid
  })
}

function prepareGroupForDB (group: APGroup, host: string): IndexableGroup {
  return Object.assign(group, {
    elasticSearchId: `${host}:${group.preferredUsername}`,
    host,
    handle: `${group.preferredUsername}@${host}`,
    url: 'https://' + host + '/@' + group.preferredUsername
  })
}

async function fetchAllRelations<T extends APEvent|APPost> (entity: T): Promise<T> {
  if (typeof entity.actor === 'string') {
    entity.actor = await getActor(entity.actor)
  }
  if (typeof entity.attributedTo === 'string') {
    entity.attributedTo = await getActor(entity.attributedTo)
  }
  return entity
}

function filterLocal ({ id }: APObject, host: string): boolean {
  const url = new URL(id)
  return url.host === host
}

export {
  getEvent,
  getGroup,

  fetchOutbox,

  prepareEventForDB,
  prepareGroupForDB,
  preparePostForDB
}
