export interface ProviderOptions {
  endpoint: string
  userAgent: string
}
