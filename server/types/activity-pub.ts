import { APActor } from './actor.model'

export interface APActivity<O, T> {
  actor: string | APActor
  attributedTo: string | APActor
  cc: string | (string | APActor)[]
  to: string | (string | APActor)[]
  published: string
  object: O
  type: T
}
export interface OrderedCollectionPage<T> {
  id: string
  next: string
  orderedItems: APActivity<T, 'Create'>[]
  partOf: string
  type: 'OrderedCollectionPage'
}

export interface APObject {
  id: string
}

export interface APMedia {
  mediaType: string | null
  type: string
  url: string
}
