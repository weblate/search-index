import { APObject } from './activity-pub'
import { Actor, APActor, DBActor } from './actor.model'
import { Location, APLocation, DBLocation } from './address.model'
import { IndexableDoc } from './indexable-doc.model'

export interface APEvent extends APObject {
  actor: APActor
  anonymousParticipationEnabled: boolean
  attachment: { mediaType: string; name: string; type: string; url: string }[]
  attributedTo: APActor
  cc: string | (string | APActor)[]
  to: string | (string | APActor)[]
  commentsEnabled: boolean
  content: string
  draft: boolean
  endTime: string
  'ical:status': string
  joinMode: string
  maximumAttendeeCapacity: number
  mediaType: string
  name: string
  published: string
  repliesModerationOption: string
  startTime: string
  tag: any[]
  type: 'Event'
  updated: string
  url?: string
  uuid: string
  location?: APLocation
}

export interface Event {
  id: string
  creator: Actor
  // anonymousParticipationEnabled: boolean
  group?: Actor | null
  // commentsEnabled: boolean
  // description: string
  endTime: string
  status: string
  // joinMode: string
  // maximumAttendeeCapacity: number
  // mediaType: string
  name: string
  published: string
  location?: Location
  startTime: string
  tags: any[]
  updated: string
  url?: string
  uuid: string
  banner: string | null
}

export interface IndexableEvent extends APEvent, IndexableDoc {}

export interface DBEvent extends Event {
  indexedAt: Date
  host: string
  url: string
  score?: number
  creator: DBActor
  group?: DBActor
  location?: DBLocation
}

// Results from the search API
export interface EnhancedEvent extends Event {
  score: number
}
