export interface Address {
  addressCountry: string | null
  addressLocality: string | null
  addressRegion: string | null
  postalCode: string | null
  streetAddress: string | null
}

export interface AutoCompleteLocation {
  name: string
  location: {
    lon: number
    lat: number
  }
}

export interface Location extends AutoCompleteLocation {
  address: Address
  id: string
}

export interface APAddress {
  addressCountry: string | null
  addressLocality: string | null
  addressRegion: string | null
  postalCode: string | null
  streetAddress: string | null
  type: 'PostalAddress'
}
export interface APLocation {
  address: APAddress
  id: string
  latitude: number
  longitude: number
  name: string
  type: 'Place'
}

export interface DBAddress extends Address {}

export interface DBLocation extends Location {}
