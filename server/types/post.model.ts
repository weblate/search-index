import { APObject } from './activity-pub'
import { Actor, APActor } from './actor.model'
import { Group } from './group.model'
import { IndexableDoc } from './indexable-doc.model'

export interface Post {
  id: string
  creator: Actor
  group?: Group
  description: string
  title: string
  published: string
  tags: any[]
  updated: string
  url?: string
  uuid: string
}

export interface DBPost extends Post {
  indexedAt: Date

  host: string

  // Added by the query
  score?: number
}

// Results from the search API
export interface EnhancedPost extends Post {
  score: number
}

export interface APPost extends APObject {
  actor: APActor
  attachment: { mediaType: string; name: string; type: string; url: string }[]
  attributedTo: APActor
  cc: string | (string | APActor)[]
  to: string | (string | APActor)[]
  commentsEnabled: boolean
  content: string
  draft: boolean
  mediaType: string
  name: string
  published: string
  repliesModerationOption: string
  tag: any[]
  type: 'Post'
  updated: string
  url?: string
  uuid: string
}

export interface IndexablePost extends APPost, IndexableDoc {}
