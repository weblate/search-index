export type CommonSearch = {
  blockedAccounts?: string[]
  blockedHosts?: string[]
  fromHost?: string
}

export interface CommonQuery {
  start?: number
  count?: number
  sort?: string

  isLive?: boolean

  categoryOneOf?: number[]

  licenceOneOf?: number[]

  languageOneOf?: string[]

  tagsOneOf?: string[]
  tagsAllOf?: string[]

  skipCount?: boolean
}

export interface ResultList<T> {
  total: number
  data: T[]
}
