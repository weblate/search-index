import { CommonSearch } from './common-search.model'

export interface BasicGroupsSearchQuery {
  search?: string

  start?: number
  count?: number
  sort?: string

  latlon?: string
  distance?: string

  host?: string
  handles?: string[]
}

export type GroupsSearchQuery = BasicGroupsSearchQuery & CommonSearch
