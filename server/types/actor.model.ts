import { APMedia } from './activity-pub'
import { APLocation } from './address.model'

export interface Actor {
  id: string
  name: string
  displayName: string
  avatar: string
  description: string
  url: string
  host: string
}

export interface APActor {
  id: string
  preferredUsername: string
  name: string
  icon: APMedia
  image: APMedia
  summary: string
  type: 'Group' | 'Person'
}
export interface APGroup extends APActor {
  type: 'Group'
  location?: APLocation
}

export interface APPerson extends APActor {
  type: 'Person'
}

export interface DBActor extends Actor {
  id: string
  name: string
  displayName: string
  description: string
  url: string
  host: string

  handle: string
  avatar: any
}
