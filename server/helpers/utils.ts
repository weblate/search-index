import * as express from 'express'

function badRequest (req: express.Request, res: express.Response) {
  return res.type('json').status(400).end()
}

function buildUrl (host: string, path: string) {
  return 'https://' + host + path
}

// ---------------------------------------------------------------------------

export {
  badRequest,
  buildUrl
}
