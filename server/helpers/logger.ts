import pino from 'pino'
import { CONFIG } from '../initializers/constants'

export const logger = pino({
  prettyPrint: true,
  level: CONFIG.LOG.LEVEL
})
