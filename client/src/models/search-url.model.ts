export interface SearchUrl {
  search?: string
  nsfw?: string
  host?: string
  publishedDateRange?: string
  eventStartDateRange?: string
  categoryOneOf?: number[]
  licenceOneOf?: number[]
  languageOneOf?: string[]
  contentType?: string

  tagsAllOf?: string[]
  tagsOneOf?: string[]

  locationName?: string
  latlon?: string
  distance?: string

  sort?: string
  page?: number | string
}
