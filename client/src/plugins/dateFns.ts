import { App } from 'vue'

export default function install(app: App, options: { locale: string }) {
  import(
    `../../node_modules/date-fns/esm/locale/${convertLocale(
      options.locale
    )}/index.js`
  ).then((localeEntity) => {
    app.config.globalProperties.$dateFnsLocale = localeEntity.default
  })
}

function convertLocale(locale: string) {
  switch (locale) {
    case 'fr_FR':
      return 'fr'
    default:
      return locale.replace('_', '-')
  }
}
