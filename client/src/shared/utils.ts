import {
  addDays,
  addMonths,
  addWeeks,
  eachWeekendOfInterval,
  endOfDay,
  endOfMonth,
  endOfToday,
  endOfWeek, format,
  startOfDay,
  startOfMonth,
  startOfWeek,
} from 'date-fns'

function buildApiUrl(path: string) {
  const normalizedPath = path.startsWith('/') ? path : '/' + path

  const base = import.meta.env.VITE_API_URL || ''
  return base + normalizedPath
}

function durationToString(duration: number) {
  const hours = Math.floor(duration / 3600)
  const minutes = Math.floor((duration % 3600) / 60)
  const seconds = duration % 60

  const minutesPadding = minutes >= 10 ? '' : '0'
  const secondsPadding = seconds >= 10 ? '' : '0'
  const displayedHours = hours > 0 ? hours.toString() + ':' : ''

  return (
    displayedHours +
    minutesPadding +
    minutes.toString() +
    ':' +
    secondsPadding +
    seconds.toString()
  ).replace(/^0/, '')
}

function pageToAPIParams(page: number, itemsPerPage: number) {
  const start = (page - 1) * itemsPerPage
  const count = itemsPerPage

  return { start, count }
}

function startDateRangeToAPIParams(durationRange: string, locale: Locale) {
  if (!durationRange) {
    return { startDateMin: undefined, startDateMax: undefined }
  }

  switch (durationRange) {
    case 'past':
      return { startDateMin: undefined, startDateMax: new Date().toISOString() }

    case 'future':
      return { startDateMin: new Date().toISOString(), startDateMax: undefined }

    case 'today':
      return {
        startDateMin: new Date().toISOString(),
        startDateMax: endOfToday().toISOString(),
      }

    case 'tomorrow':
      return {
        startDateMin: startOfDay(addDays(new Date(), 1)).toISOString(),
        startDateMax: endOfDay(addDays(new Date(), 1)).toISOString(),
      }

    case 'week_end':
      return weekend(locale)

    case 'week':
      return {
        startDateMin: new Date().toISOString(),
        startDateMax: endOfWeek(addDays(new Date(), 1), {
          locale,
        }).toISOString(),
      }

    case 'next_week':
      return {
        startDateMin: startOfWeek(addWeeks(new Date(), 1), {
          locale,
        }).toISOString(),
        startDateMax: endOfWeek(addWeeks(new Date(), 1), {
          locale,
        }).toISOString(),
      }

    case 'month':
      return {
        startDateMin: new Date().toISOString(),
        startDateMax: endOfMonth(new Date()).toISOString(),
      }

    case 'next_month':
      return {
        startDateMin: startOfMonth(addMonths(new Date(), 1)).toISOString(),
        startDateMax: endOfMonth(addMonths(new Date(), 1)).toISOString(),
      }

    case 'any_date':
      return { startDateMin: undefined, startDateMax: undefined }

    default:
      console.error('Unknown duration range %s', durationRange)
      return { startDateMin: undefined, startDateMax: undefined }
  }
}

function weekend(locale: Locale) {
  const now = new Date()
  const endOfWeekDate = endOfWeek(now, { locale })
  const startOfWeekDate = startOfWeek(now, { locale })
  const [start, end] = eachWeekendOfInterval({
    start: startOfWeekDate,
    end: endOfWeekDate,
  })
  return { startDateMin: startOfDay(start), startDateMax: endOfDay(end) }
}

function publishedDateRangeToAPIParams(publishedDateRange: string) {
  if (!publishedDateRange) {
    return { startDate: undefined, endDate: undefined }
  }

  // today
  const date = new Date()
  date.setHours(0, 0, 0, 0)

  switch (publishedDateRange) {
    case 'published_today':
      break

    case 'last_week':
      date.setDate(date.getDate() - 7)
      break

    case 'last_month':
      date.setDate(date.getDate() - 30)
      break

    case 'last_year':
      date.setDate(date.getDate() - 365)
      break

    default:
      console.error('Unknown published date range %s', publishedDateRange)
      return { startDate: undefined, endDate: undefined }
  }

  return { startDate: date.toISOString(), endDate: undefined }
}

function extractTagsFromQuery(value: any | any[]) {
  if (!value) return []

  if (Array.isArray(value) === true) {
    return (value as any[]).map((v) => ({ text: v }))
  }

  return [{ text: value }]
}

function formatList(list: string[]): string {
  if (window.Intl && Intl.ListFormat) {
    const formatter = new Intl.ListFormat(undefined, {
      style: 'short',
      type: 'conjunction',
    })
    return formatter.format(list)
  }
  return list.join(',')
}

function formatDateTime(dateTime: Date, locale): string {
  return format(dateTime, 'PPp', { locale })
}

export {
  buildApiUrl,
  durationToString,
  publishedDateRangeToAPIParams,
  pageToAPIParams,
  startDateRangeToAPIParams,
  extractTagsFromQuery,
  formatList,
  formatDateTime,
}
