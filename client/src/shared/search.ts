import axios from 'axios'
import { ResultList } from '../../../server/types/search-query/common-search.model'
import { EventsSearchQuery } from '../../../server/types/search-query/event-search.model'
import { GroupsSearchQuery } from '../../../server/types/search-query/group-search.model'
import { EnhancedGroup } from '../../../server/types/group.model'
import { EnhancedEvent } from '../../../server/types/event.model'
import { buildApiUrl } from './utils'
import { AutoCompleteLocation } from '../../../server/types/address.model'

const baseEventsPath = '/api/v1/search/events'
const baseGroupsPath = '/api/v1/search/groups'
const baseLocationsPath = '/api/v1/search/locations'

function searchEvents(params: EventsSearchQuery) {
  const options = {
    params,
  }

  if (params.search) Object.assign(options.params, { search: params.search })
  return axios
    .get<ResultList<EnhancedEvent>>(buildApiUrl(baseEventsPath), options)
    .then((res) => res.data)
}

function searchGroups(params: GroupsSearchQuery) {
  const options = {
    params,
  }

  if (params.search) Object.assign(options.params, { search: params.search })

  return axios
    .get<ResultList<EnhancedGroup>>(buildApiUrl(baseGroupsPath), options)
    .then((res) => res.data)
}

async function searchLocations(search: string) {
  const res = await axios.get<ResultList<AutoCompleteLocation>>(
    buildApiUrl(baseLocationsPath),
    { params: { search } }
  )
  return res.data
}

export { searchEvents, searchGroups, searchLocations }
