// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { Language } from 'vue3-gettext'
import { Locale } from 'date-fns'

declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    $gettext: Language['$gettext']
    $pgettext: Language['$pgettext']
    $ngettext: Language['$ngettext']
    $npgettext: Language['$npgettext']
    $gettextInterpolate: Language['interpolate']
    $dateFnsLocale: Locale
  }
}
