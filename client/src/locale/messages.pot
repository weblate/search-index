msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"

#: src/components/SearchWarning.vue:7
msgid "<strong>%{indexName}</strong> displays events and groups that match your search but is not the publisher, nor the owner. If you notice any problems with an event, report it to the administrators on the Mobilizon website where the event is published."
msgstr ""

#: src/components/Header.vue:13
msgid "A search engine of <a class=\"font-bold underline\" href=\"https://joinmobilizon.org\" target=\"_blank\">Mobilizon</a> events and groups"
msgstr ""

#: src/views/Search.vue:746
msgid "Any"
msgstr ""

#: src/views/Search.vue:791
msgid "Any distance"
msgstr ""

#: src/views/Search.vue:418
msgid "Apply filters"
msgstr ""

#: src/views/Search.vue:675
msgid "Arts"
msgstr ""

#: src/views/Search.vue:711
#: src/views/Search.vue:715
msgid "Auto, Boat & Air"
msgstr ""

#: src/views/Search.vue:479
msgid "Best match"
msgstr ""

#: src/views/Search.vue:679
msgid "Book Clubs"
msgstr ""

#: src/views/Search.vue:683
msgid "Business"
msgstr ""

#: src/views/Search.vue:844
msgid "Català"
msgstr ""

#: src/views/Search.vue:281
#: src/views/Search.vue:285
msgid "Category"
msgstr ""

#: src/views/Search.vue:687
msgid "Causes"
msgstr ""

#: src/views/Search.vue:845
msgid "Čeština"
msgstr ""

#: src/components/InterfaceLanguageDropdown.vue:10
msgid "Change interface language"
msgstr ""

#: src/components/Footer.vue:83
msgid "Chat"
msgstr ""

#: src/views/Search.vue:691
msgid "Comedy"
msgstr ""

#: src/components/Footer.vue:69
#: src/views/Search.vue:719
msgid "Community"
msgstr ""

#: src/views/Search.vue:695
msgid "Crafts"
msgstr ""

#: src/components/Footer.vue:22
msgid "Create groups"
msgstr ""

#: src/components/PostResult.vue:32
msgid "Created by"
msgstr ""

#: src/views/Search.vue:848
msgid "Deutsch"
msgstr ""

#: src/components/Header.vue:19
msgid "Developed by <a class=\"font-bold underline\" href=\"https://framasoft.org\" target=\"_blank\"><span class=\"text-frama-violet\">Frama</span><span class=\"text-frama-orange\">soft</span></a>"
msgstr ""

#: src/components/Footer.vue:64
msgid "Discover Mobilizon"
msgstr ""

#: src/components/GroupResult.vue:124
msgid "Discover this group on %{host}"
msgstr ""

#: src/views/Search.vue:255
#: src/views/Search.vue:259
msgid "Distance"
msgstr ""

#: src/views/Search.vue:840
msgid "English"
msgstr ""

#: src/views/Search.vue:851
msgid "Español"
msgstr ""

#: src/views/Search.vue:846
msgid "Esperanto"
msgstr ""

#: src/views/Search.vue:843
msgid "Euskara"
msgstr ""

#: src/views/Search.vue:226
#: src/views/Search.vue:230
#: src/views/Search.vue:482
msgid "Event date"
msgstr ""

#: src/views/Search.vue:662
msgid "Events"
msgstr ""

#: src/views/Search.vue:658
msgid "Everything"
msgstr ""

#: src/views/Home.vue:55
#: src/views/Search.vue:85
msgid "Explore!"
msgstr ""

#: src/views/Search.vue:723
msgid "Family & Education"
msgstr ""

#: src/views/Search.vue:727
msgid "Fashion & Beauty"
msgstr ""

#: src/views/Search.vue:731
msgid "Film & Media"
msgstr ""

#: src/views/Search.vue:699
msgid "Food & Drink"
msgstr ""

#: src/views/Search.vue:841
msgid "Français"
msgstr ""

#: src/views/Search.vue:853
msgid "Gàidhlig"
msgstr ""

#: src/views/Search.vue:735
msgid "Games"
msgstr ""

#: src/components/Footer.vue:13
msgid "Getting started"
msgstr ""

#: src/components/ActorMiniature.vue:73
msgid "Go on this group page"
msgstr ""

#: src/views/Home.vue:54
#: src/views/Search.vue:84
msgid "Go!"
msgstr ""

#: src/views/Search.vue:666
msgid "Groups"
msgstr ""

#: src/views/Search.vue:703
msgid "Health"
msgstr ""

#: src/components/Footer.vue:32
msgid "Host it yourself"
msgstr ""

#: src/components/PostResult.vue:40
msgid "In"
msgstr ""

#: src/views/Search.vue:754
msgid "In the future"
msgstr ""

#: src/views/Search.vue:750
msgid "In the past"
msgstr ""

#: src/components/Footer.vue:38
msgid "Install Mobilizon"
msgstr ""

#: src/views/Search.vue:849
msgid "Italiano"
msgstr ""

#: src/views/Search.vue:44
msgid "Keyword, event title, group name, etc."
msgstr ""

#: src/views/Home.vue:19
msgid "Keyword, event title, group name, posts, etc."
msgstr ""

#: src/views/Search.vue:122
#: src/views/Search.vue:123
#: src/views/Search.vue:161
#: src/views/Search.vue:181
#: src/views/Search.vue:183
#: src/views/Search.vue:307
#: src/views/Search.vue:311
msgid "Language"
msgstr ""

#: src/views/Search.vue:494
msgid "Least recently published"
msgstr ""

#: src/components/search/LocationAutocomplete.vue:4
#: src/views/Search.vue:60
msgid "Location"
msgstr ""

#: src/views/Search.vue:488
msgid "Most recently published"
msgstr ""

#: src/views/Search.vue:707
msgid "Music"
msgstr ""

#: src/views/Search.vue:850
msgid "Nederlands"
msgstr ""

#: src/views/Search.vue:782
msgid "Next month"
msgstr ""

#: src/components/Pagination.vue:31
msgid "Next page"
msgstr ""

#: src/views/Search.vue:774
msgid "Next week"
msgstr ""

#: src/views/Search.vue:525
msgid "No event found"
msgstr ""

#: src/views/Search.vue:530
msgid "No event or group found"
msgstr ""

#: src/views/Search.vue:442
msgid "No events found"
msgstr ""

#: src/views/Search.vue:528
msgid "No group found"
msgstr ""

#: src/views/Search.vue:445
msgid "No groups found"
msgstr ""

#: src/views/Search.vue:448
msgid "No results found"
msgstr ""

#: src/views/Search.vue:852
msgid "Occitan"
msgstr ""

#: src/views/Search.vue:451
msgid "One event found"
msgid_plural "%{eventsCount} events found"
msgstr[0] ""
msgstr[1] ""

#: src/views/Search.vue:457
msgid "One group found"
msgid_plural "%{groupsCount} groups found"
msgstr[0] ""
msgstr[1] ""

#: src/views/Search.vue:795
#: src/views/Search.vue:801
#: src/views/Search.vue:807
#: src/views/Search.vue:813
#: src/views/Search.vue:819
#: src/views/Search.vue:828
msgid "one kilometer"
msgid_plural "%{number} kilometers"
msgstr[0] ""
msgstr[1] ""

#: src/views/Search.vue:463
msgid "One result found"
msgid_plural "%{resultsCount} results found"
msgstr[0] ""
msgstr[1] ""

#: src/views/Search.vue:217
msgid "Online events"
msgstr ""

#: src/components/Footer.vue:19
msgid "Open an account"
msgstr ""

#: src/views/Search.vue:857
msgid "Polski"
msgstr ""

#: src/views/Search.vue:855
msgid "Português (Portugal)"
msgstr ""

#: src/components/Pagination.vue:8
msgid "Previous page"
msgstr ""

#: src/components/PostResult.vue:99
msgid "Read the post on %{host}"
msgstr ""

#: src/views/Search.vue:128
#: src/views/Search.vue:129
#: src/views/Search.vue:167
#: src/views/Search.vue:187
#: src/views/Search.vue:189
msgid "Reset"
msgstr ""

#: src/components/Footer.vue:58
msgid "Resources"
msgstr ""

#: src/views/Home.vue:59
msgid "Search for your favorite events and groups on %{tagStart}one Mobilizon website%{tagEnd} indexed by %{indexName}!"
msgid_plural "Search for your favorite events and groups on %{tagStart}%{instancesCount} Mobilizon websites{%tagEnd%} indexed by %{indexName}!"
msgstr[0] ""
msgstr[1] ""

#: src/views/Search.vue:474
msgid "Sort by"
msgstr ""

#: src/components/Footer.vue:75
msgid "Source code"
msgstr ""

#: src/views/Search.vue:858
msgid "suomi"
msgstr ""

#: src/views/Search.vue:856
msgid "svenska"
msgstr ""

#: src/views/Search.vue:778
msgid "This month"
msgstr ""

#: src/views/Search.vue:770
msgid "This week"
msgstr ""

#: src/views/Search.vue:766
msgid "This weekend"
msgstr ""

#: src/views/Search.vue:758
msgid "Today"
msgstr ""

#: src/views/Search.vue:762
msgid "Tomorrow"
msgstr ""

#: src/views/Search.vue:532
msgid "Try to reset some filters to see more events"
msgstr ""

#: src/components/PostResult.vue:49
msgid "Updated on"
msgstr ""

#: src/components/EventResult.vue:47
msgid "View more events near %{locality}"
msgstr ""

#: src/components/GroupResult.vue:49
msgid "View more groups near %{locality}"
msgstr ""

#: src/components/Footer.vue:46
msgid "Why should I have my own Mobilizon website?"
msgstr ""

#: src/views/Home.vue:31
#: src/views/Search.vue:13
msgid "Your search"
msgstr ""

#: src/views/Search.vue:847
msgid "ελληνικά"
msgstr ""

#: src/views/Search.vue:859
msgid "русский"
msgstr ""

#: src/views/Search.vue:842
msgid "日本語"
msgstr ""

#: src/views/Search.vue:854
msgid "简体中文（中国）"
msgstr ""
