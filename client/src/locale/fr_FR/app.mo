��    g      T  �   �      �  �   �  �   �	     .
     2
     ?
     M
     R
  
   c
  
   n
     y
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
  �   �
     �     �     �     �     �  	   �     �  
          
        "     +     >     O     \  	   i     s     y     �     �     �  	   �     �     �     �     �     �     �     �  &     -   +     Y     b     {     �     �  
   �  
   �  	   �  	   �     �     �     �               "     3  +   ;  +   g  .   �     �     �     �     �     �          $  	   *  �   4     &     .  
   :  	   E     O     \     b  ,   k  
   �  !   �  !   �  +   �       "        B     H  	   P     Z     k  	   z     �  �  �  '  D  t   l     �     �     �            
   (     3  
   D     O  
   W     b      i     �     �     �  	   �     �  
   �     �  �   �     �      �     �     �     �  
   �     �     �                    !     7     G     W  	   n     x     }     �     �     �     �     �     �     �     �     �          "  5   *  >   `     �     �     �     �     �     �     �               .  #   H     l     �     �     �     �  ;   �  1     8   ?     x     �     �     �     �     �     �  
   �  
    	          
   %     0     >     J     V  ;   ]     �  1   �  -   �  9   	     C  #   S     w          �     �     �     �     �         \   ;      <   /           c           G          d              P          ?   Q   '   E      @         :          R   A   D   V          H   N   7      (   `   0   M   L   1               F   T   K   4   ,   W      _   5           #   *      C   =      .              "   >             ]          f       %   e          	   b   [   6      !           S   I   2           ^       J   -   
      $   8   B   )           3   &       O          U       +          Z      g      a      Y         9      X            <strong>%{indexName}</strong> displays events and groups that match your search but is not the publisher, nor the owner. If you notice any problems with an event, report it to the administrators on the Mobilizon website where the event is published. A search engine of <a class="font-bold underline" href="https://joinmobilizon.org" target="_blank">Mobilizon</a> events and groups Any Any distance Apply filters Arts Auto, Boat & Air Best match Book Clubs Business Català Category Causes Change interface language Chat Comedy Community Crafts Create groups Created by Deutsch Developed by <a class="font-bold underline" href="https://framasoft.org" target="_blank"><span class="text-frama-violet">Frama</span><span class="text-frama-orange">soft</span></a> Discover Mobilizon Discover this group on %{host} Distance English Español Esperanto Euskara Event date Events Everything Explore! Family & Education Fashion & Beauty Film & Media Food & Drink Français Games Getting started Go on this group page Go! Groups Gàidhlig Health Host it yourself In In the future In the past Install Mobilizon Italiano Keyword, event title, group name, etc. Keyword, event title, group name, posts, etc. Language Least recently published Location Most recently published Music Nederlands Next month Next page Next week No event found No event or group found No events found No group found No groups found No results found Occitan One event found %{eventsCount} events found One group found %{groupsCount} groups found One result found %{resultsCount} results found Online events Open an account Polski Português (Portugal) Previous page Read the post on %{host} Reset Resources Search for your favorite events and groups on %{tagStart}one Mobilizon website%{tagEnd} indexed by %{indexName}! Search for your favorite events and groups on %{tagStart}%{instancesCount} Mobilizon websites{%tagEnd%} indexed by %{indexName}! Sort by Source code This month This week This weekend Today Tomorrow Try to reset some filters to see more events Updated on View more events near %{locality} View more groups near %{locality} Why should I have my own Mobilizon website? Your search one kilometer %{number} kilometers suomi svenska Čeština ελληνικά русский 日本語 简体中文（中国） Project-Id-Version: 
PO-Revision-Date: 2022-02-07 17:42+0100
Last-Translator: chocobozzz <chocobozzz@framasoft.org>
Language-Team: French (France) <https://weblate.framasoft.org/projects/peertube-search-index/client/fr_FR/>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Poedit 3.0.1
Generated-By: easygettext
 <strong>%{indexName}</strong> affiche les événements et groupes correspondants à votre recherche, mais n'est ni l'éditeur, ni le propriétaire. Si vous constatez le moindre problème sur un événement, signalez-le à un administrateur directement sur le site Mobilizon où il est hébergé. Un moteur de recherche d'événements et de groupes <a href="https://joinpeertube.org" target="_blank">Mobilizon</a> Toutes N'importe où Appliquer les filtres Arts Auto, bateau et avions Pertinence Clubs de lecture Entreprise Catalan Catégorie Causes Changer la langue de l'interface Chat Humour Communauté Artisanat Créer des groupes Créé par Allemand Développé par <a class="font-bold underline" href="https://framasoft.org" target="_blank"><span class="text-frama-violet">Frama</span><span class="text-frama-orange">soft</span></a> Découvrir Mobilizon Découvrir ce groupe sur %{host} Distance Anglais Espagnol Espéranto Basque Date de l'événement Événements Tout Explorer ! Famille et éducation Mode et beauté Film et médias Nourriture et boissons Français Jeux Pour commencer Aller sur la page de ce groupe Go ! Groupes Gaélique écossais Santé Hébergez-le vous-mêmes Dans Dans le futur Dans le passé Installer Mobilizon Italien Mot-clé, titre de l'événement, nom du groupe, etc. Mot-clé, titre de l'événement, nom du groupe, billets, etc. Langue Le moins récemment publié Emplacement Publié dernièrement Musiques Néerlandais Le mois prochain Page suivante La semaine prochaine Aucun événement trouvé Aucun événement ou groupe trouvé Aucun événement trouvé Aucun groupe trouvé Aucun groupe trouvé Aucun résultat trouvé Occitane Un événement trouvé %{eventsCount} événements trouvés Un groupe trouvé %{groupsCount} groupes trouvés Un résultat trouvé %{resultsCount} résultats trouvés Événements en ligne Ouvrir un compte Polonais Portugais (Portugal) Page précédente Lire le billet sur %{host} Réinitialiser Ressources Cherchez parmi vos événements et vos groupes favoris sur %{tagStart}un site Mobilizon%{tagEnd} indexé par %{indexName} ! Cherchez parmi vos événements et vos groupes favoris sur %{tagStart}%{instancesCount} sites Mobilizon%{tagEnd} indexés par %{indexName} ! Trier par Code source Ce mois-ci Cette semaine Ce week-end Aujourd'hui Demain Essayez d'enlever des filtres pour voir plus d'événements Mise à jour le Afficher plus d'événements près de %{locality} Afficher plus de groupes près de %{locality} Pourquoi devrais-je avoir mon propre site web Mobilizon ? Votre recherche un kilomètre %{number} kilomètres Finnois Suédois Tchèque Grec Russe Japonais Chinois simplifié 