// ############# I18N ##############

import { createGettext } from 'vue3-gettext'

async function buildTranslationsPromise(defaultLanguage, currentLanguage) {
  let translations = {}

  // No need to translate anything
  if (currentLanguage === defaultLanguage) return Promise.resolve(translations)

  translations = await import('../locale/translations.json')
  return translations

  // Fetch translations from server
  // const fromRemote = import('./translations/' + currentLanguage + '.json').then(
  //   (module) => {
  //     const remoteTranslations = module.default
  //     try {
  //       localStorage.setItem(
  //         'translations-v1-' + currentLanguage,
  //         JSON.stringify(remoteTranslations)
  //       )
  //     } catch (err) {
  //       console.error('Cannot save translations in local storage.', err)
  //     }

  //     return Object.assign(translations, remoteTranslations)
  //   }
  // )

  // // If we have a cache, try to
  // const fromLocalStorage = localStorage.getItem(
  //   'translations-v1-' + currentLanguage
  // )
  // if (fromLocalStorage) {
  //   try {
  //     Object.assign(translations, JSON.parse(fromLocalStorage))

  //     return Promise.resolve(translations)
  //   } catch (err) {
  //     console.error('Cannot parse translations from local storage.', err)
  //   }
  // }

  // return fromRemote
}

const availableLanguages = {
  en_US: 'English',
  fr_FR: 'Français',
  de: 'Deutsch',
  el: 'ελληνικά',
  es: 'Español',
  gd: 'Gàidhlig',
  gl: 'galego',
  ru: 'русский',
  it: 'Italiano',
  ja: '日本語',
  sv: 'svenska',
  nl: 'Nederlands',
  oc: 'Occitan',
  sq: 'Shqip',
  pt_BR: 'Português (Brasil)',
  bn: 'বাংলা',
  pl: 'Polski',
}
const aliasesLanguages = {
  en: 'en_US',
  fr: 'fr_FR',
  br: 'pt_BR',
  pt: 'pt_BR',
}
const allLocales = Object.keys(availableLanguages).concat(
  Object.keys(aliasesLanguages)
)

const defaultLanguage = 'en_US'
let currentLanguage = defaultLanguage

const basePath = import.meta.env.BASE_URL
const startRegexp = new RegExp('^' + basePath)

const paths = window.location.pathname.replace(startRegexp, '').split('/')

const localePath = paths.length !== 0 ? paths[0] : ''
const languageFromLocalStorage = localStorage.getItem('language')

if (allLocales.includes(localePath)) {
  currentLanguage = aliasesLanguages[localePath]
    ? aliasesLanguages[localePath]
    : localePath
  localStorage.setItem('language', currentLanguage)
} else if (languageFromLocalStorage) {
  currentLanguage = languageFromLocalStorage
} else {
  const navigatorLanguage =
    (window.navigator as any).userLanguage || window.navigator.language
  const snakeCaseLanguage = navigatorLanguage.replace('-', '_')
  currentLanguage = aliasesLanguages[snakeCaseLanguage]
    ? aliasesLanguages[snakeCaseLanguage]
    : snakeCaseLanguage
}

async function generateGettext() {
  let translations
  try {
    translations = await buildTranslationsPromise(
      defaultLanguage,
      currentLanguage
    )
  } catch (err) {
    console.error('Cannot load translations.', err)
    translations = { default: {} }
  }

  return createGettext({
    translations,
    availableLanguages,
    defaultLanguage: currentLanguage,
    mutedLanguages: ['en_US'],
  })
}

export { generateGettext, currentLanguage, allLocales }
