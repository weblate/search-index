import './scss/main.scss'
import './scss/index.css'
import { createApp } from 'vue'
import VueMatomo from 'vue-matomo'
import App from './App.vue'
import Search from './views/Search.vue'
import Home from './views/Home.vue'
import { createRouter, createWebHistory } from 'vue-router'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import dateFns from './plugins/dateFns'
import 'flowbite'
import { generateGettext, currentLanguage, allLocales } from './boot/i18n'

async function main() {
  const app = createApp(App)

  document.documentElement.setAttribute(
    'lang',
    currentLanguage.replace('_', '-')
  )

  const gettext = await generateGettext()
  app.use(gettext)

  // Routes
  const routes = [
    {
      name: 'SEARCH',
      path: '/:locale?/search',
      component: Search,
    },
    {
      name: 'HOME',
      path: '/:locale?',
      component: Home,
    },
    {
      path: '/*',
      // Don't want to create a 404 page
      component: Home,
    },
  ]
  const router = createRouter({
    history: createWebHistory(),
    routes,
  })
  router.beforeEach((to, from, next) => {
    const locale = to.params.locale as string
    console.log('route locale param', gettext.current, locale)
    if (locale && !allLocales.includes(locale)) {
      return next('en_US')
    }
    if (gettext.current !== locale) {
      console.log('updating gettext locale')
      gettext.current = locale
    }
    return next()
  })

  // Stats Matomo
  if (
    !(
      navigator.doNotTrack === 'yes' ||
      navigator.doNotTrack === '1' ||
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      window.doNotTrack === '1'
    )
  ) {
    app.use(VueMatomo, {
      // Configure your matomo server and site
      host: 'https://stats.framasoft.org/',
      siteId: 77,

      // Enables automatically registering pageviews on the router
      router,

      // Require consent before sending tracking information to matomo
      // Default: false
      requireConsent: false,

      // Whether to track the initial page view
      // Default: true
      trackInitialView: true,

      // Changes the default .js and .php endpoint's filename
      // Default: 'piwik'
      trackerFileName: 'p',

      enableLinkTracking: true,
    })

    const _paq = []

    // CNIL conformity
    _paq.push([
      function piwikCNIL() {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        const { getVisitorInfo } = this

        function getOriginalVisitorCookieTimeout() {
          const now = new Date()
          const nowTs = Math.round(now.getTime() / 1000)
          const visitorInfo = getVisitorInfo()
          const createTs = parseInt(visitorInfo[2], 10)
          const cookieTimeout = 33696000 // 13 months in seconds
          return createTs + cookieTimeout - nowTs
        }

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        this.setVisitorCookieTimeout(getOriginalVisitorCookieTimeout())
      },
    ])
  }

  app.use(router)

  app.mount('#app')

  app.use(dateFns, { locale: currentLanguage })
}

main()
