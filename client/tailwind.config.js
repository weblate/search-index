module.exports = {
  content: ['./src/**/*.{html,ts,vue}', './node_modules/flowbite/**/*.js'],
  darkMode: 'media',
  theme: {
    extend: {
      colors: {
        'frama-violet': '#725794',
        'frama-orange': '#cc4e13',
      },
      transitionProperty: {
        height: 'height',
        spacing: 'margin, padding',
      },
    },
  },
  plugins: [
    require('flowbite/plugin'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/forms'),
  ],
}
